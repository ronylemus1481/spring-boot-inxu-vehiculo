package com.ingesis.inxu.vehiculo.controller;

import com.ingesis.inxu.vehiculo.model.CatMarcaVehiculo;
import com.ingesis.inxu.vehiculo.repository.MarcaVehiculoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class MarcaVehiculoController {
    private MarcaVehiculoRepository repository;

    @Autowired
    public MarcaVehiculoController(MarcaVehiculoRepository repository){
        this.repository=repository;
    }

    @GetMapping("/marcas")
    public List<CatMarcaVehiculo> all() {
        return repository.findAll();
    }

    @PostMapping("/marcas")
    public CatMarcaVehiculo newMarcaVehiculo(@RequestBody CatMarcaVehiculo newMarca) {
        return repository.save(newMarca);
    }

    @GetMapping("/marcas/{id}")
    public CatMarcaVehiculo single(@PathVariable Integer id) {

        return repository.findById(id)
                .orElseThrow(() -> new RuntimeException("Could not find  " + id));
    }

    @PutMapping("/marcas/{id}")
    public CatMarcaVehiculo replace(@RequestBody CatMarcaVehiculo newMarca,
                                    @PathVariable Integer id) {

        return repository.findById(id)
                .map(marca -> {
                    marca.setCodigoMarca(newMarca.getCodigoMarca());
                    marca.setDescripMarca(newMarca.getDescripMarca());
                    return repository.save(marca);
                })
                .orElseGet(() -> {
                    newMarca.setIdeMarcaVehiculo(id);
                    return repository.save(newMarca);
                });
    }

    @DeleteMapping("/marcas/{id}")
    void delete(@PathVariable Integer id) {
        repository.deleteById(id);
    }

}
