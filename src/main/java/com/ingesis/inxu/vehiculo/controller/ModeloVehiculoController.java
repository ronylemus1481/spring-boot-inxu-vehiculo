package com.ingesis.inxu.vehiculo.controller;

import com.ingesis.inxu.vehiculo.model.CatModeloVehiculo;
import com.ingesis.inxu.vehiculo.repository.ModeloVehiculoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ModeloVehiculoController {
    private ModeloVehiculoRepository repository;

    @Autowired
    public ModeloVehiculoController(ModeloVehiculoRepository repository){
        this.repository = repository;
    }

    @GetMapping("/modelos")
    public List<CatModeloVehiculo> all() {
        return repository.findAll();
    }

    @PostMapping("/modelos")
    public CatModeloVehiculo newMarcaVehiculo(@RequestBody CatModeloVehiculo newModelo) {
        return repository.save(newModelo);
    }

    @GetMapping("/modelos/{ideMarca}/{ideModelo}")
    public CatModeloVehiculo single(@PathVariable Integer ideMarca,@PathVariable Integer ideModelo) {

        return repository.findByIdeMarcaVehiculoAndIdeModeloVehiculo(ideMarca,ideModelo);
    }

    @GetMapping("/modelosByMarca/{id}")
    public List<CatModeloVehiculo> modelosByMarca(@PathVariable Integer id) {

        return repository.findByIdeMarcaVehiculo(id);
    }

    @PutMapping("/modelos/{id}")
    public CatModeloVehiculo replace(@RequestBody CatModeloVehiculo newModelo,
                                    @PathVariable Integer id) {

        return repository.findById(id)
                .map(modelo -> {
                    modelo.setCodigoModelo(newModelo.getCodigoModelo());
                    modelo.setDescripModelo(newModelo.getDescripModelo());
                    return repository.save(modelo);
                })
                .orElseGet(() -> {
                    newModelo.setIdeModeloVehiculo(id);
                    return repository.save(newModelo);
                });
    }

    @DeleteMapping("/modelos/{ideMarca}/{ideModelo}")
    void delete(@PathVariable Integer ideMarca,@PathVariable Integer ideModelo) {
        repository.deleteByIdeMarcaVehiculoAndIdeModeloVehiculo(ideMarca,ideModelo);
    }
}
