package com.ingesis.inxu.vehiculo.repository;

import com.ingesis.inxu.vehiculo.model.CatMarcaVehiculo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MarcaVehiculoRepository extends JpaRepository<CatMarcaVehiculo,Integer> {
}
