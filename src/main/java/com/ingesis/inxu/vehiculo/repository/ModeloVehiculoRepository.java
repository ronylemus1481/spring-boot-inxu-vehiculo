package com.ingesis.inxu.vehiculo.repository;

import com.ingesis.inxu.vehiculo.model.CatModeloVehiculo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ModeloVehiculoRepository extends JpaRepository<CatModeloVehiculo,Integer> {
    List<CatModeloVehiculo> findByIdeMarcaVehiculo(Integer id);
    CatModeloVehiculo findByIdeMarcaVehiculoAndIdeModeloVehiculo(Integer ideMarcaVehiculo,Integer ideModeloVehiculo);
    void deleteByIdeMarcaVehiculoAndIdeModeloVehiculo(Integer ideMarcaVehiculo,Integer ideModeloVehiculo);
}
