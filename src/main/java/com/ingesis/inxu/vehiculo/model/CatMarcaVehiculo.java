package com.ingesis.inxu.vehiculo.model;

import lombok.Data;

import javax.persistence.*;

@Table(name = "cat_marca_vehiculo")
@Entity
@Data
public class CatMarcaVehiculo  {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "cfgMarcaVehiculoGen")
    @TableGenerator(name = "cfgMarcaVehiculoGen", table = "cfg_secuencia",
            pkColumnName = "cod_secuencia", pkColumnValue = "cat_marca_vehiculo",
            valueColumnName = "correlativo", initialValue = 1, allocationSize = 1)
    @Column(name ="ide_marca_vehiculo")
    private Integer ideMarcaVehiculo;
    @Column(name ="codigo_marca")
    private String codigoMarca;
    @Column(name ="descrip_marca")
    private String descripMarca;
}
