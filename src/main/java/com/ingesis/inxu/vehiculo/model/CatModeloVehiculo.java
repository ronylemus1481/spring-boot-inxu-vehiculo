package com.ingesis.inxu.vehiculo.model;


import lombok.Data;

import javax.persistence.*;
@IdClass(CatModeloVehiculoPK.class)
@Entity
@Table(name ="cat_modelo_vehiculo")
@Data
public class CatModeloVehiculo {
    @Id
    @Column(name ="ide_marca_vehiculo")
    private Integer ideMarcaVehiculo;
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "cfgModeloVehiculoGen")
    @TableGenerator(name = "cfgModeloVehiculoGen", table = "cfg_secuencia",
            pkColumnName = "cod_secuencia", pkColumnValue = "cat_modelo_vehiculo",
            valueColumnName = "correlativo", initialValue = 1, allocationSize = 1)
    @Id
    @Column(name ="ide_modelo_vehiculo")
    private Integer ideModeloVehiculo;
    @Column(name ="codigo_modelo")
    private String codigoModelo;
    @Column(name ="descrip_modelo")
    private String descripModelo;
}
