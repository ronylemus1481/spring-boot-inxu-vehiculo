package com.ingesis.inxu.vehiculo.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class CatModeloVehiculoPK implements Serializable  {
    private Integer ideMarcaVehiculo;
    private Integer ideModeloVehiculo;

    public  CatModeloVehiculoPK(Integer ideMarcaVehiculo,Integer ideModeloVehiculo){
        this.ideMarcaVehiculo = ideMarcaVehiculo;
        this.ideModeloVehiculo = ideModeloVehiculo;
    }
}
